#include <iostream>
#include <string>

int main()
{
	std::string value = "Skillbox Homework";
	std::cout << value << "\n";
	std::cout << value.length() << "\n";
	std::cout << value.substr(0,1 ) << "\n";
	std::cout << value.substr(value.length()-1, 1) << "\n";
}